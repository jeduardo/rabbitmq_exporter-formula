[![pipeline status](https://gitlab.com/depositsolutions/rabbitmq_exporter-formula/badges/master/pipeline.svg)](https://gitlab.com/depositsolutions/rabbitmq_exporter-formula/commits/master)

# rabbitmq_exporter-formula

A saltstack formula created to setup [rabbitmq_exporter monitoring server](https://github.com/kbudde/rabbitmq_exporter).

# rabbitmq_exporter ENVs

```bash
./rabbitmq_exporter --help
```
Environment variable|default|description
--------------------|-------|------------
RABBIT_URL | <http://localhost:15672>| url to rabbitMQ management plugin (must start with http(s)://)
RABBIT_USER | guest | username for rabbitMQ management plugin
RABBIT_PASSWORD | guest | password for rabbitMQ management plugin
RABBIT_USER_FILE| | location of file with username (useful for docker secrets)
RABBIT_PASSWORD_FILE | | location of file with password (useful for docker secrets)
PUBLISH_PORT | 9090 | Listening port for the exporter
PUBLISH_ADDR | "" | Listening host/IP for the exporter
OUTPUT_FORMAT | TTY | Log ouput format. TTY and JSON are suported
LOG_LEVEL | info | log level. possible values: "debug", "info", "warning", "error", "fatal", or "panic"
CAFILE | ca.pem | path to root certificate for access management plugin. Just needed if self signed certificate is used. Will be ignored if the file does not exist
SKIPVERIFY | false | true/0 will ignore certificate errors of the management plugin
SKIP_VHOST | ^$ |regex, matching vhost names are not exported. First performs INCLUDE_VHOST, then SKIP_VHOST
INCLUDE_VHOST | .* | reqgex vhost filter. Only queues in matching vhosts are exported
INCLUDE_QUEUES | .* | reqgex queue filter. just matching names are exported
SKIP_QUEUES | ^$ |regex, matching queue names are not exported (useful for short-lived rpc queues). First performed INCLUDE, after SKIP
RABBIT_CAPABILITIES | | comma-separated list of extended scraping capabilities supported by the target RabbitMQ server
RABBIT_EXPORTERS | exchange,node,overview,queue | List of enabled modules. Just "connections" is not enabled by default
RABBIT_TIMEOUT | 30 | timeout in seconds for retrieving data from management plugin.
MAX_QUEUES | 0 | max number of queues before we drop metrics (disabled if set to 0)


# pillar.example

```saltstack
rabbitmq_exporter:
  bin_dir: '/usr/bin'
  dist_dir: '/opt/rabbitmq_exporter/dist'
  version: '0.29.0'
  service: True
  service_user: 'rabbitmq_exporter'
  service_group: 'rabbitmq_exporter'
  ssl: False
  cafile: /srv/certs/bundleca.pem
  rabbitmq_url: 'http://localhost:15672'
  rabbitmq_user:
  rabbitmq_password:
```

# Available states

## init

The essential rabbitmq_exporter state running both ``install``.

## install

- Download rabbitmq_exporter release from Github into the dist dir  (defaults to /opt/rabbitmq_exporter/dist).
- link the binary (defaults to /usr/bin/rabbitmq_exporter).
- Register the appropriate service definition with systemd.

This state can be called independently.

## uninstall

Remove the service, binaries, and configuration files. The data itself will be kept and needs to be removed manually, just to be on the safe side.

This state must always be called independently.

# Testing

## Prerequisites
The tests are using test kitchen with [inspec](https://www.inspec.io/downloads/) as verifier, so you need to have
- Ruby installed
- Docker installed

If you want to add tests you might want to take a look at the [documentation](http://testinfra.readthedocs.io/en/latest/modules.html#) for the modules.

## Running the test
If you run the tests for the first time you might need to run ``bundle install`` first. Afterwards you can run ``kitchen test ``.  
