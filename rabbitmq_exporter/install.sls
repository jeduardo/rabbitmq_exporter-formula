{% from slspath+"/map.jinja" import rabbitmq_exporter with context %}

rabbitmq_exporter-create-user:
  user.present:
    - name: {{ rabbitmq_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

rabbitmq_exporter-create-group:
  group.present:
    - name: {{ rabbitmq_exporter.service_group }}
    - system: True
    - members:
      - {{ rabbitmq_exporter.service_user }}
    - require:
      - user: {{ rabbitmq_exporter.service_user }}

rabbitmq_exporter-bin-dir:
  file.directory:
   - name: {{ rabbitmq_exporter.bin_dir }}
   - makedirs: True

rabbitmq_exporter-dist-dir:
 file.directory:
   - name: {{ rabbitmq_exporter.dist_dir }}
   - makedirs: True

rabbitmq_exporter-install-binary:
 archive.extracted:
   - name: {{ rabbitmq_exporter.dist_dir }}/rabbitmq_exporter-{{ rabbitmq_exporter.version }}
   - source: https://github.com/kbudde/rabbitmq_exporter/releases/download/v{{ rabbitmq_exporter.version }}/rabbitmq_exporter-{{ rabbitmq_exporter.version }}.{{ grains['kernel'] | lower }}-{{ rabbitmq_exporter.arch }}.tar.gz
   - skip_verify: True
   - options: --strip-components=1
   - user: {{ rabbitmq_exporter.service_user }}
   - group: {{ rabbitmq_exporter.service_group }}
   - enforce_toplevel: False
   - unless:
     - '[[ -f {{ rabbitmq_exporter.dist_dir }}/rabbitmq_exporter-{{ rabbitmq_exporter.version }}/rabbitmq_exporter ]]'
 file.symlink:
   - name: {{ rabbitmq_exporter.bin_dir }}/rabbitmq_exporter
   - target: {{ rabbitmq_exporter.dist_dir }}/rabbitmq_exporter-{{ rabbitmq_exporter.version }}/rabbitmq_exporter
   - mode: 0755
   - unless:
     - '[[ -f {{ rabbitmq_exporter.bin_dir }}/rabbitmq_exporter ]] && {{ rabbitmq_exporter.bin_dir }}/rabbitmq_exporter -v | grep {{ rabbitmq_exporter.version }}'


#rabbitmq_exporter-symlink-binary:
#  file.symlink:
#    - name: {{ rabbitmq_exporter.bin_dir }}/rabbitmq_exporter
#    - target: {{ rabbitmq_exporter.dist_dir }}/rabbitmq_exporter-{{ rabbitmq_exporter.version }}
#    - mode: 0755
