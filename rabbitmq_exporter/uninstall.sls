{% from slspath+"/map.jinja" import rabbitmq_exporter with context %}

rabbitmq_exporter-remove-service:
  service.dead:
    - name: rabbitmq_exporter
  file.absent:
    - name: /etc/systemd/system/rabbitmq_exporter.service
    - require:
      - service: rabbitmq_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: rabbitmq_exporter-remove-service

rabbitmq_exporter-remove-symlink:
   file.absent:
    - name: {{ rabbitmq_exporter.bin_dir}}/rabbitmq_exporter
    - require:
      - rabbitmq_exporter-remove-service

rabbitmq_exporter-remove-binary:
   file.absent:
    - name: {{ rabbitmq_exporter.dist_dir}}
    - require:
      - rabbitmq_exporter-remove-symlink

rabbitmq_exporter-remove-env:
    file.absent:
      - name: {{ rabbitmq_exporter.config_dir }}
      - require:
        - rabbitmq_exporter-remove-binary
