rabbitmq:
  version: "3.6.6-1"
  enabled: True
  running: True
  # Optional block: special configuration for user and group
  config:
    user: rabbitmq
    group: rabbitmq
    # Configuration for the AMQP broker
    rabbit:
      tcp_listeners:
        '0.0.0.0': 5672
    # Configuration for the management plugin
    rabbitmq_management:
      listener:
        port: 15672
        ip: '0.0.0.0'
        ssl: false

  plugin:
    rabbitmq_management:
      - enabled
  vhost:
    vh_name: '/virtual/host'
    other_vhost: '/other'
  user:
    admin:
      - password: password
      - force: True
      - tags: monitoring, user, administrator
      - perms:
        - '/':
          - '.*'
          - '.*'
          - '.*'
      - runas: root
    user1:
      - password: password
      - force: True
      - tags: monitoring, user
      - perms:
        - '/':
          - '.*'
          - '.*'
          - '.*'
      - runas: root
    user2:
      - password: password
      - force: True
      - tags: monitoring, user
      - perms:
        - '/':
          - '.*'
          - '.*'
          - '.*'
      - runas: root
