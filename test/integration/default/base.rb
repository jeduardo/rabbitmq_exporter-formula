describe user('rabbitmq_exporter') do
  it { should exist }
end

describe group('rabbitmq_exporter') do
  it { should exist }
end

describe file('/usr/bin/rabbitmq_exporter') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
end

describe file('/opt/rabbitmq_exporter/dist') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/systemd/system/rabbitmq_exporter.service.d') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/systemd/system/rabbitmq_exporter.service.d/env.conf') do
 it { should exist }
 it { should be_owned_by 'root' }
end

describe service('rabbitmq_exporter') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
